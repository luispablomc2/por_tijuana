﻿using Microsoft.EntityFrameworkCore;
using por_tijuana.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace por_tijuana.Context
{
    public class PorTijuanaContext :DbContext
    {
        public PorTijuanaContext(DbContextOptions<PorTijuanaContext> opt) : base(opt)
        {

        }

        public DbSet<Feed> Feed { get; set; }
        public DbSet<Categories> Categories { get; set; }
        public DbSet<Images> Images { get; set; }
        public DbSet<User> User { get; set; }
    }
}
