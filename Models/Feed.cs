﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace por_tijuana.Models
{
    public class Feed
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public bool Public { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public int ModifyByID { get; set; }
        public int CategoryID { get; set; }
        public int ImageID { get; set; }
    }
}
