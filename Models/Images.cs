﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace por_tijuana.Models
{
    public class Images
    {
        public int ID { get; set; }
        public string UrlContainer { get; set; }
    }
}
