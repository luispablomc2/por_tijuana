﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace por_tijuana.Models
{
    public class User
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
