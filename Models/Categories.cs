﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace por_tijuana.Models
{
    public class Categories
    {
        public int ID { get; set; }
        public string Description { get; set; }
    }
}
